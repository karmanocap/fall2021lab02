package JavaCode;

public class BikeStore {
    public static void main(String[] args) {
        
        Bicycle[] bikes = new Bicycle[4];

        for (int i = 0; i < bikes.length; i++) {
            bikes[i] = new Bicycle((int) ((Math.random() * 8) + 1), (double) ((Math.random() * 30) + 1), "Yamaha");
            System.out.println(bikes[i]);
        }
    }
}
