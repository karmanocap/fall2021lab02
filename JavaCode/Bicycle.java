package JavaCode;

public class Bicycle {
    private int numberGears;
    private double maxSpeed;
    private String manufacturer;

    public Bicycle(int numberGears, double maxSpeed, String manufacturer) {
        this.numberGears = numberGears;
        this.maxSpeed = maxSpeed;
        this.manufacturer = manufacturer;
    }

    public int getNumberGears() {
        return this.numberGears;
    }

    public double getMaxSpeed() {
        return this.maxSpeed;
    }

    public string getManufacturer() {
        return this.manufacturer;
    }

    public String toString() {
        return ("Numbers of gears: " + this.numberGears + "\nMaxSpeed: " + this.maxSpeed + "\nManufacturer: " + this.manufacturer);
    }
}
